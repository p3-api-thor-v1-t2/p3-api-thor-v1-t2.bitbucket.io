/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9944165270798436, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.9996834107218235, 500, 1500, "getDashboardData"], "isController": false}, {"data": [0.49671322925225964, 500, 1500, "getChildCheckInCheckOutByArea"], "isController": false}, {"data": [1.0, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [1.0, 500, 1500, "findAllLevels"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (health_check_type)"], "isController": false}, {"data": [1.0, 500, 1500, "getAllEvents"], "isController": false}, {"data": [0.9999611891640146, 500, 1500, "addOrUpdateUserDevice"], "isController": false}, {"data": [0.0, 500, 1500, "getHomefeed"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (Relation_Child)"], "isController": false}, {"data": [0.9996591683708248, 500, 1500, "me"], "isController": false}, {"data": [1.0, 500, 1500, "getNotificationCount"], "isController": false}, {"data": [1.0, 500, 1500, "dismissChildCheckInByVPS"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (non_parent_relationship)"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (guardian_rejection_reason)"], "isController": false}, {"data": [1.0, 500, 1500, "addClassesToArea"], "isController": false}, {"data": [1.0, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9999208171668382, 500, 1500, "getCentreHolidaysOfYear"], "isController": false}, {"data": [1.0, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.9999611921763427, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.999633377328054, 500, 1500, "getChildCheckInCheckOut"], "isController": false}, {"data": [0.9996831833734634, 500, 1500, "getClassAttendanceSummaries"], "isController": false}, {"data": [0.9997672072631334, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (checkout_decline_reason)"], "isController": false}, {"data": [0.9999266216612855, 500, 1500, "getAllCentreClasses"], "isController": false}, {"data": [1.0, 500, 1500, "getAllArea"], "isController": false}, {"data": [1.0, 500, 1500, "findAllConfigByCategory (checkin_decline_reason)"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 157608, 0, 0.0, 47.46813613522185, 2, 6455, 16.0, 63.0, 128.0, 414.9800000000032, 520.4830736003646, 1617.288300292922, 584.7999492154347], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllConfigByCategory", 1467, 0, 0.0, 8.594410361281524, 2, 307, 5.0, 14.0, 20.0, 75.63999999999987, 4.913766249426392, 4.569621266107071, 4.391582417166027], "isController": false}, {"data": ["getDashboardData", 4738, 0, 0.0, 166.56922752216073, 69, 658, 159.0, 220.0, 249.0, 344.0, 15.843292515732944, 137.5302999729982, 42.17657753700001], "isController": false}, {"data": ["getChildCheckInCheckOutByArea", 1217, 0, 0.0, 1002.2900575184866, 517, 1574, 999.0, 1213.0, 1289.1, 1466.6399999999999, 4.052195744029088, 21.82507331494251, 13.45455618134658], "isController": false}, {"data": ["getAllClassInfo", 1212, 0, 0.0, 33.713696369637, 8, 318, 26.0, 57.700000000000045, 82.0, 155.95999999999913, 4.0647816186014065, 4.489519541638859, 3.9734828127148516], "isController": false}, {"data": ["findAllLevels", 6814, 0, 0.0, 16.830789550924656, 4, 318, 10.0, 29.0, 52.0, 136.0, 22.793642935275287, 14.513139837694812, 19.187617392780563], "isController": false}, {"data": ["findAllConfigByCategory (health_check_type)", 6816, 0, 0.0, 16.289172535211176, 5, 345, 10.0, 29.0, 51.0, 117.82999999999993, 22.795376712328768, 22.461460061269264, 21.904932309503426], "isController": false}, {"data": ["getAllEvents", 7890, 0, 0.0, 21.366793409378992, 6, 467, 12.0, 37.0, 71.0, 185.1800000000003, 26.395110381073135, 13.068672815628984, 28.663440179446606], "isController": false}, {"data": ["addOrUpdateUserDevice", 12883, 0, 0.0, 35.28588061786861, 9, 504, 21.0, 72.0, 113.0, 224.47999999999956, 43.09574863099161, 20.79033185909165, 52.79077890890783], "isController": false}, {"data": ["getHomefeed", 255, 0, 0.0, 5584.1372549019625, 3797, 6455, 5555.0, 6136.4, 6243.0, 6364.44, 0.8449696142299511, 159.2941007607626, 2.567090302606814], "isController": false}, {"data": ["findAllConfigByCategory (Relation_Child)", 6816, 0, 0.0, 16.64069835680746, 5, 423, 10.0, 28.0, 50.0, 134.0, 22.79514800458846, 18.20940533960289, 21.83792987548953], "isController": false}, {"data": ["me", 1467, 0, 0.0, 63.91820040899786, 23, 517, 49.0, 106.0, 140.5999999999999, 265.23999999999955, 4.909441387896068, 8.45867212285651, 11.263137179480744], "isController": false}, {"data": ["getNotificationCount", 8138, 0, 0.0, 23.703366920619384, 7, 499, 14.0, 45.0, 75.0, 170.0, 27.229867865878347, 14.146767289694608, 29.329426756574083], "isController": false}, {"data": ["dismissChildCheckInByVPS", 1212, 0, 0.0, 21.492574257425716, 7, 245, 14.0, 40.0, 59.0, 121.0, 4.064822516165383, 2.258675792673929, 3.104190632462236], "isController": false}, {"data": ["findAllConfigByCategory (non_parent_relationship)", 6816, 0, 0.0, 17.642312206572836, 5, 357, 10.0, 31.0, 58.0, 147.0, 22.79331850786697, 18.630866788168607, 22.03650910428545], "isController": false}, {"data": ["findAllConfigByCategory (guardian_rejection_reason)", 6815, 0, 0.0, 16.548055759354362, 5, 387, 10.0, 28.0, 51.0, 125.84000000000015, 22.793175759967625, 21.70248668551605, 22.080889017468635], "isController": false}, {"data": ["addClassesToArea", 1212, 0, 0.0, 69.75000000000006, 14, 380, 54.0, 140.70000000000005, 175.3499999999999, 269.7399999999998, 4.0647816186014065, 2.2268969609720597, 2.9572874080645], "isController": false}, {"data": ["getCountCheckInOutChildren", 6818, 0, 0.0, 21.840129070108524, 6, 486, 12.0, 40.0, 74.0, 185.42999999999847, 22.794823187998784, 14.313546201057829, 26.57912000631889], "isController": false}, {"data": ["getCentreHolidaysOfYear", 12629, 0, 0.0, 31.89056932457049, 10, 757, 19.0, 62.0, 99.0, 208.70000000000073, 42.24353335095014, 76.72150724226226, 41.66559632217175], "isController": false}, {"data": ["findAllChildrenByParent", 255, 0, 0.0, 53.22745098039214, 19, 419, 36.0, 112.4, 173.39999999999998, 328.5199999999997, 0.8572033656157242, 1.8416478558150324, 1.3134297662608116], "isController": false}, {"data": ["findAllSchoolConfig", 12884, 0, 0.0, 42.715926730829025, 10, 525, 27.0, 85.0, 126.0, 255.14999999999964, 43.085833910196605, 939.6414481274517, 25.79425946849992], "isController": false}, {"data": ["getChildCheckInCheckOut", 6819, 0, 0.0, 46.256929168499575, 14, 820, 31.0, 88.0, 129.0, 249.80000000000018, 22.734243725495425, 11.500330322076787, 66.80404235353099], "isController": false}, {"data": ["getClassAttendanceSummaries", 7891, 0, 0.0, 25.91293879102774, 6, 667, 13.0, 54.0, 97.39999999999964, 232.0, 26.393069770553215, 14.949199674727405, 28.944743508135993], "isController": false}, {"data": ["getLatestMobileVersion", 12887, 0, 0.0, 20.357104058353432, 8, 828, 15.0, 33.0, 46.0, 99.0, 42.96927098615594, 23.376639048495893, 25.230893900752555], "isController": false}, {"data": ["findAllConfigByCategory (checkout_decline_reason)", 6814, 0, 0.0, 16.473437041385424, 5, 374, 10.0, 29.0, 49.0, 119.85000000000036, 22.79310921558789, 15.737039272871717, 22.036306761164074], "isController": false}, {"data": ["getAllCentreClasses", 6814, 0, 0.0, 31.805253889051944, 8, 571, 23.0, 52.5, 80.0, 169.85000000000036, 22.79295672883941, 31.27353926173767, 25.86466378799941], "isController": false}, {"data": ["getAllArea", 1212, 0, 0.0, 41.72689768976897, 11, 380, 30.5, 71.0, 101.0, 238.8699999999999, 4.064754354015958, 7.5380551936292015, 3.997273080560614], "isController": false}, {"data": ["findAllConfigByCategory (checkin_decline_reason)", 6817, 0, 0.0, 18.335044741088474, 5, 385, 10.0, 33.0, 63.0, 158.0, 22.7933850032433, 15.113973063674026, 22.01431422676526], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 157608, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
